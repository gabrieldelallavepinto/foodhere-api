import * as functions from 'firebase-functions';

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.auth = require('./auth/auth');
exports.business = require('./business/business');
exports.order = require('./order/order');
exports.order = require('./modifier/modifier');
exports.bookings = require('./bookings/bookings');
exports.stripe_payment = require('./payments/stripe_payment');
