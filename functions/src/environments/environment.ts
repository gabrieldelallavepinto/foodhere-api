// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAi1yPX6E9PYTRiEV3tNeBGRfDmiQfV_w4",
    authDomain: "foodhere-5adb8.firebaseapp.com",
    databaseURL: "https://foodhere-5adb8.firebaseio.com",
    projectId: "foodhere-5adb8",
    storageBucket: "foodhere-5adb8.appspot.com",
    messagingSenderId: "530510102400",
    appId: "1:530510102400:web:68f8b9548524c043730b65"
  },
  db: {
    permission_collection: "permissions",
    rol_collection: "roles",
    user_collection: "users",
    business_collection: "business",
    client_collection: "clients",
    employee_collection: "employees",
    store_collection: "stores",
    category_collection: "categories",
    format_collection: "formats",
    variant_collection: "variants",
    modifier_collection: "modifiers",
    product_collection: "products",
    order_collection: "orders",
    promotion_collection: "promotions",
    programmedMessage_collection: "programmed_messages",

    setting_collection: "settings",
    paymentMethods_collection: "payment_methods",
    channel_collection: "channels",
    imageSettings_collection: "image_settings",

    devicePos_collection: "devices_pos",
    tracking_collection: "tracking",

    masterModifier_collection: "master_modifiers",
  }
};