export const environment = {
  production: false,
  test: false,
  firebaseConfig: {
    apiKey: "AIzaSyD3KTDUUdzRTXimO5-SF8mwkrm8avLwDn4",
    authDomain: "hellovan-test.firebaseapp.com",
    databaseURL: "https://hellovan-test.firebaseio.com",
    projectId: "hellovan-test",
    storageBucket: "hellovan-test.appspot.com",
    messagingSenderId: "411956115018",
    appId: "1:411956115018:web:3e6fb5ed6b760b29d718b3",
    function: "https://us-central1-hellovan-test.cloudfunctions.net/"
  },
  db: {
    main_collection : 'companies',
    user_collection : 'users',
    vanner_collection : 'vanners',
    permission_collection : 'permissions',
    rol_collection : 'roles',
    business_collection : 'business',
    center_collection : 'centers',

    product_collection : 'products',
    productBusiness_collection : 'products',
    order_collection : 'orders',
    tracking_collection : 'tracking',
    service_collection : 'services',

    reserve_collection : 'reserves',
    log_collection : 'log',

    setting_collection: 'settings',
    incidence_collection : 'orderIncidences',
  },
  hb_config: {
    default_logo: '../../../assets/images/logoHellovan-dark.png',
    vannerRolName: 'VANNER',
    customerRolName: 'CUSTOMER',
    superAdminRolName: 'SUPERADMIN'
  }
};
