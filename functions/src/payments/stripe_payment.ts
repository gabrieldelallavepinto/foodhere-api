import * as functions from "firebase-functions";
const admin = require('firebase-admin');
const stripe = require('stripe')(functions.config().stripe.token);
const firestore = admin.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);
const cors = require('cors')({origin: true});




exports.createIntent = functions.https.onRequest( (req, res)  => {
    cors(req, res, async () => {

        const paymentMethodId = req.body.payment_method_id;
        const businessId = req.body.business_id;
        const clientId = req.body.client_id;
        const orderId = req.body.order_id;

        const snapshot = await firestore.collection('business').doc(businessId).collection('clients').doc(clientId).get();
        const customerId = snapshot.data().customer_id;

        const paymentIntent = await stripe.paymentIntents.create({
            amount: 100,
            currency: 'eur',
            payment_method_types: ['card'],
            customer: customerId,
            payment_method : paymentMethodId ,
            metadata: { uid: clientId, businessId: businessId, orderId: orderId }
        });
        console.log(paymentIntent);
        return res
            .status(200)
            .json({
                ok: true,
                intent: paymentIntent,
                body: req.body,
                message: `Stripe intent ${req.body.payment_method_id} has been created.`,
            });
    });
});


exports.createCard = functions.firestore.document('business/{businessId}/clients/{userId}/stripe_cards/{paymentMethodId}')
.onWrite(async (tokenSnap, context) => {
    let customer;

    if (!tokenSnap.after.exists){
        console.log('detaching payment method');
        await stripe.paymentMethods.detach(context.params.paymentMethodId);
        console.log('detach payment method');
        return null;
    }


    console.log(functions.config().stripe.token);
    const snapshot = await firestore.collection('business').doc(context.params.businessId).collection('clients').doc(context.params.userId).get();
    const customerId = snapshot.data().customer_id;
    const customerEmail = snapshot.data().email;
    if (customerId === 'new') {
        customer = await stripe.customers.create({
            email: customerEmail,
            payment_method: context.params.paymentMethodId,
            metadata: {
                "businessId": context.params.businessId,
                "uid":  context.params.userId
            }
        });

        await firestore.collection('business')
            .doc(context.params.businessId)
            .collection('clients')
            .doc(context.params.userId)
            .update({
            customer_id: customer.id
        });
    } else {
        console.log('retrieve');
        customer = await stripe.customers.retrieve(customerId);

        await stripe.paymentMethods.attach(
            context.params.paymentMethodId,
            {customer: customer.id},
        );
    }


});
