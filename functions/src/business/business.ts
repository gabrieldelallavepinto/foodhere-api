import * as functions from "firebase-functions";
import { environment } from '../environments/environment';
import { createUser, getUser } from "../auth/auth";


/**
 * Activador cuando se crea un pedido
 */
exports.onCreate = functions.firestore.document(`${environment.db.business_collection}/{business_id}`).onCreate(async (snap, context) => {
    const business_id = context.params.business_id;
    const business = snap.data();

    const business_ref = snap.ref;

    const rol_ref = business_ref.collection(environment.db.rol_collection);
    const employee_ref = business_ref.collection(environment.db.employee_collection);

    // Añadimos el rol administrador
    const rol = { id: 'admin', business_id: business_id, name: 'admin', description: 'admin', root: true , permissions: [] };
    await rol_ref.doc('admin').set(rol);

    const auth = await createUser(business.email, business.email).catch(async (error) => {
        return await getUser(business.email);
    });

    const user_id = auth.uid;
    const user = { id: user_id, business_id: business_id, store_id: null, status: 'enabled', rol: 'admin', first_name: business.name, last_name: '', documentation: '', gender: null, email: business.email, phone: '', optional_phone: '', avatar_url: null, about: '', address: business.address };
    await employee_ref.doc(user_id).set(user);

    return null;
});


