import {FunctionsErrorCode} from "firebase-functions/lib/providers/https";
import * as functions from "firebase-functions";

/**
 * Menejador de errores
 * @param error
 * @param code
 * @param message
 */
export function handleError(
    error: any,
    code: FunctionsErrorCode = 'failed-precondition',
    message: string = 'Another user is already using the email provided.'
) {
    throw new functions.https.HttpsError(
        code,
        message,
        {
            type: error.code,
            message: error.message
        });
}