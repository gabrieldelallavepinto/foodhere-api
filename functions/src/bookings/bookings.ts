import * as functions from 'firebase-functions';
import * as firebaseAdmin from 'firebase-admin';
import { BookingInterface, BookingShiftInterface, BookingRoomInterface, BookingTableInterface } from '../interfaces/booking.interface';

const cors = require('cors')({ origin: true });
const adminFirestore = firebaseAdmin.firestore();

/**
 * Función que verifica la disponibilidad de reserva
 */
// exports.available = functions.https.onCall( async (data, context) => {
//     console.log('entra en la función');
// });

exports.createBooking = functions.https.onRequest((req, resp) => {
    return cors(req, resp, async () => {
        console.log('entra en la función');
        const booking: BookingInterface = req.body;

        const businessRef = adminFirestore.collection('business').doc(booking.businessId);
        const storeRef = businessRef.collection('stores').doc(booking.storeId);
        const shiftsRef = storeRef.collection('shifts');
        const roomsRef = storeRef.collection('rooms');
        const tablesRef = storeRef.collection('tables');
        const bookingsRef = storeRef.collection('bookings');

        // Obtenemos la Jornada
        const shiftsSnapshot = await shiftsRef.get();
        const shifts = shiftsSnapshot.docs.map(doc => doc.data() as BookingShiftInterface);

        // Obtenemos todas las salas
        const roomsSnapshot = await roomsRef.get();
        const rooms = roomsSnapshot.docs.map(doc => doc.data()  as BookingRoomInterface);

        // Obtenemos todas las mesas de la sala
        const tablesSnapshot = await tablesRef.get();
        const tables = tablesSnapshot.docs.map(doc => doc.data() as BookingTableInterface);

        // Obtenemos todas las reservas del día
        const bookingsSnapshot = await bookingsRef.get();
        const bookings = bookingsSnapshot.docs.map(doc => doc.data());

        let availableBookings = [];

        shifts.forEach(async shift => {
            rooms.forEach(room => {
                const tablesFilter = tables.filter(tables => tables.roomId == room.id && tables.maxDiners >= booking.numDiners && booking.numDiners >= tables.minDiners );

                shift.hours.forEach(hour => {
                    tablesFilter.forEach(table => {
                        const bookingFind = bookings.find(booking => booking.hour == hour && booking.tableId == table.id);
                        if(!bookingFind) {
                            availableBookings.push({
                                shiftId: shift.id,
                                shiftLabel: shift.name,
                                roomId: room.id,
                                roomLabel: room.name,
                                hour: hour,
                                tableId: table.id,
                                tableLabel: table.name,
                            });
                        }
                    });
                });

            });

            if(booking.shiftId) {
                booking.shift = shifts.find(shiftFind => shiftFind.id == booking.shiftId);
            }

            if(booking.roomId) {
                booking.room = rooms.find(roomsFind => roomsFind.id == booking.roomId);
            }

            if(!booking.tableId) {
                // Buscamos la primera mesa disponible
                const availableBookingFind = availableBookings.find(availableBooking => availableBooking.shiftId == booking.shiftId && availableBooking.roomId == booking.roomId && availableBooking.hour == booking.hour);
                
                if(availableBookingFind) {
                    booking['tableId'] = availableBookingFind.tableId;
                    booking['table'] = tables.find(table => table.id == availableBookingFind.tableId);
                }
            }

            booking['id'] = adminFirestore.collection('bookings').doc().id;
            const bookingRef = bookingsRef.doc(booking.id);
            await bookingRef.set(booking);

            resp.send(booking);

        });
    });
});

exports.available = functions.https.onRequest((req, resp) => {
    return cors(req, resp, async () => {
        console.log('entra en la función');

        const booking: BookingInterface = req.body;
        console.log(JSON.stringify(booking));

        const businessRef = adminFirestore.collection('business').doc(booking.businessId);
        const storeRef = businessRef.collection('stores').doc(booking.storeId);
        const shiftsRef = storeRef.collection('shifts');
        const roomsRef = storeRef.collection('rooms');
        const tablesRef = storeRef.collection('tables');
        const bookingsRef = storeRef.collection('bookings');

        // Obtenemos la Jornada
        const shiftsSnapshot = await shiftsRef.get();
        const shifts = shiftsSnapshot.docs.map(doc => doc.data());

        // Obtenemos todas las salas
        const roomsSnapshot = await roomsRef.get();
        const rooms = roomsSnapshot.docs.map(doc => doc.data());

        // Obtenemos todas las mesas de la sala
        const tablesSnapshot = await tablesRef.get();
        const tables = tablesSnapshot.docs.map(doc => doc.data());

        // Obtenemos todas las reservas del día
        const bookingsSnapshot = await bookingsRef.get();
        const bookings = bookingsSnapshot.docs.map(doc => doc.data());

        let availableBookings = [];

        // console.log(JSON.stringify(shifts));


        shifts.forEach(shift => {
            rooms.forEach(room => {
                const tablesFilter = tables.filter(tables => tables.roomId == room.id && tables.maxDiners >= booking.numDiners && booking.numDiners >= tables.minDiners );
                console.log(JSON.stringify(tablesFilter));

                shift.hours.forEach(hour => {
                    tablesFilter.forEach(table => {
                        const bookingFind = bookings.find(booking => booking.hour == hour && booking.tableId == table.id);
                        if(!bookingFind) {
                            availableBookings.push({
                                shiftId: shift.id,
                                shiftLabel: shift.name,
                                roomId: room.id,
                                roomLabel: room.name,
                                hour: hour,
                                tableId: table.id,
                                tableLabel: table.name,
                            });
                        }
                    });
                });

            });
        });

        resp.send(availableBookings);
    });
});