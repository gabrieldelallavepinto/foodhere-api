import * as functions from "firebase-functions";
import { environment } from '../environments/environment';
import {handleError} from '../shared/functions';

import * as firebaseAdmin from 'firebase-admin';

const cors = require('cors')({ origin: true });
const adminFirestore = firebaseAdmin.firestore();


/**
 * Activador cuando se crea un pedido
 */
exports.onCreateOrder = functions.firestore.document(`${environment.db.business_collection}/{business_id}/${environment.db.order_collection}/{order_id}`).onCreate(async (snap, context) => {
    return null;
});

/**
 * Activador cuando se actualiza un pedido
 */
exports.onUpdateOrder = functions.firestore.document(`${environment.db.business_collection}/{business_id}/${environment.db.order_collection}/{order_id}`).onUpdate(async (snap, context) => {
    const order_ref = snap.after.ref;
    const orderAfter = snap.after.data();
    const orderBefore = snap.before.data();

    if(orderAfter.status !== orderBefore.status) {
        await order_ref.collection(environment.db.tracking_collection).add(createOrderTrack(orderAfter.status));
    }
    
    return null;
});


/**
 * Función que crea un pedido
 */
exports.createOrder = functions.https.onRequest((req, res) => {
    return cors(req, res, async () => {

        console.log('entra en la función');

        const data = req.body.data;
        if (!data) return res.status(500).send('No se han pasado datos');

        const order = await _createOrder(data);
        if(!order) res.status(500).send('Algo ha salido mal');

        return res.status(200).json(order);
    });
});

/**
 * Función que genera un pedido dado los datos
 * @param data
 */
async function _createOrder(data: any) {

    if (!data.business_id || !data.store_id) return null;

    const today = new Date();
    const orderDate = data.date ? new Date(data.date) : new Date();

    const business_ref = adminFirestore.collection('business').doc(data.business_id);
    const businessSnapshot = await business_ref.get();
    const business = businessSnapshot.exists ? businessSnapshot.data() : null;

    const store_ref = business_ref.collection('stores').doc(data.store_id);
    const storeSnapshot = await store_ref.get();
    const store = storeSnapshot.exists ? storeSnapshot.data() : null;

    const order_id: string = business_ref.collection('orders').doc().id;

    const order = {

        id: order_id,
        
        business_id: data.business_id,
        business: data.business ? data.business : business,

        store_id: data.store_id,
        store: data.store ? data.store : store,

        client_id: data.client_id ? data.client_id : null,
        client: data.client ? data.client : null,

        cancellation_comment: data.cancellation_comment ? data.cancellation_comment : null,
        date: data.date ? data.date : today,

        deliveries: data.deliveries ? data.deliveries : null,
        delivery_address: data.delivery_address ? data.delivery_address : null,
        delivery_amount: data.delivery_amount ? data.delivery_amount : null,
        diners_number: data.diners_number ? data.diners_number : null,

        id_sinqro: data.id_sinqro ? data.id_sinqro : '',
        is_cancellable: data.is_cancellable ? data.is_cancellable : false,

        market_order_code: data.market_order_code ? data.market_order_code : null,
        market_order_id: data.market_order_id ? data.market_order_id : null,

        order_lines: data.order_lines ? data.order_lines : [],

        payments: data.payments ? data.payments : null,
        pos_order_id: data.pos_order_id ? data.pos_order_id : null,

        printed: data.printed ? data.printed : null,

        ref: data.ref ? data.ref : null,

        status: data.status ? data.status : 'accepted',

        viewed: false,

        total_amount: data.total_amount ? data.total_amount : 0,
        type: data.type ? data.type : 'collect',

        created_at: today,
        updated_at: today,
        deleted_at: null,
    };

    try {

        const order_ref = business_ref.collection('orders').doc(order.id);

        await adminFirestore.runTransaction(async (transaction) => {
            console.log('entra en la transaction');

            const businessSnapshot = await business_ref.get();
            const storeSnapshot = await store_ref.get();

            // si existe la empresa y la tienda
            if (businessSnapshot.exists && storeSnapshot.exists) {
                // const business = businessSnapshot.data() as FirebaseFirestore.DocumentData;
                // const center = storeSnapshot.data() as FirebaseFirestore.DocumentData;

                if (order.ref === null) {

                    const ordersCount = store.ordersCount ? (store.ordersCount + 1) : 0;
                    const code = store.code ? store.code : '';
                    const orderDate: Date = order.date.toDate();
                    const year = orderDate.getFullYear().toString();

                    order.ref = `${code}${year.substr(2)}${ordersCount.toString().padStart(5)}`;

                    transaction.update(order_ref, order);
                    transaction.update(store_ref, { orderCount: ordersCount + 1 });
                }
            }

        });

        await order_ref.collection(environment.db.tracking_collection).add(createOrderTrack('created'));

    } catch{
        console.log('algo ha ido mal en la transaction');
        return null;
    }

    return order;

};

/**
 * Función que devuelve el track según el estado
 * @param status
 */
function createOrderTrack(status: string, label: string = null, description = null, comment: string = null){

    const track = {date: new Date(), status: status, label: label, description: description, comment: comment};

    switch(status){
        case 'created': 
            track.label = "Recibido";
            track.description = "Su pedido se ha recibido correctamente";
            break;
        case 'waiting_validation': 
            track.label = "Esperando a ser validado";
            track.description = "Su pedido está en espera de validación";
            break;
        case 'accepted': 
            track.label = "Aceptado";
            track.description = "Su pedido ha sido aceptado por la tienda";
            break;
        case 'preparation': 
            track.label = "En preparación";
            track.description = "Estamos preparando su pedido";
            break;
        case 'ready': 
            track.label = "Preparado";
            track.description = "Su pedido ya terminado de prepararse";
            break;
        case 'delivering': 
            track.label = "En camino";
            track.description = "Un repartidor va en camino con su pedido";
            break;
        case 'finished': 
            track.label = "Finalizado";
            track.description = "Su pedido ya está finalizado";
            break;
    }

    return track;
}


