import * as functions from "firebase-functions";
import { environment } from '../environments/environment';
import {handleError} from '../shared/functions';

import * as firebaseAdmin from 'firebase-admin';

const cors = require('cors')({ origin: true });
const adminFirestore = firebaseAdmin.firestore();


/**
 * Activador cuando se actualiza un pedido
 */
exports.onWriteModifier = functions.firestore.document('business/{business_id}/modifiers/{modifier_id}').onWrite(async (snap, context) => {
    const modifier_ref = snap.after.ref;
    const modifier = snap.after.data();

    const productsRef = adminFirestore.collection('business').doc(modifier.business_id).collection('products');
    const modifiersRef = adminFirestore.collection('business').doc(modifier.business_id).collection('modifiers');

    const productsSnapshot = await productsRef.where('category_id', '==', modifier.category_id).get();
    const modifiersSnapshot = await modifiersRef.where('category_id', '==', modifier.category_id).orderBy('sort_order', 'desc').get();
    

    productsSnapshot.docs.forEach(productSnapshot => {
        // Se obtiene el producto
        let product = productSnapshot.data();

        if(product.modifiers){
            let modifiers = Object.assign([], modifiersSnapshot.docs);

            // Recorremos todos los modificadores para actualizarlos en el producto
            modifiers.forEach(modifier => {
                // Buscamos si tiene el modificador
                const modifierFind = product.modifiers.find(modifierFind => modifierFind.id === modifier.id);
                // Si el modificador existe lo actualizamos con los datos del producto
                if(modifierFind) {
                    modifier = _updateModifier(modifier, modifierFind);
                }
            });

            product.modifiers = modifiers;
        }
        
        if(product.use_variants === true) {

            product.variants.forEach(variant => {

                let modifiers = Object.assign([], modifiersSnapshot.docs);

                 // Recorremos todos los modificadores para actualizarlos en el producto
                modifiers.forEach(modifier => {
                    // Buscamos si tiene el modificador
                    const modifierFind = variant.modifiers.find(modifierFind => modifierFind.id === modifier.id);
                    // Si el modificador existe lo actualizamos con los datos del producto
                    if(modifierFind) {
                        modifier = _updateModifier(modifier, modifierFind);
                    }
                });

                variant.modifiers = modifiers;
            });
        }
    });

    return null;
});

function _updateModifier(modifier, modifierFind) {
    modifier.active = modifierFind.active;
    modifier.name = modifierFind.name;
    modifier.short_name = modifierFind.short_name;
    modifier.description = modifierFind.description;

    modifier.options.forEach((option) => {
        // Buscamos la opcion del modificador
        const optionFind = modifierFind.options.find(optionFind => optionFind.id === option.id);
        if(optionFind) {
            option.active = optionFind.active;
            option.name = optionFind.name;
            option.icon = optionFind.icon;
            option.short_name = optionFind.short_name;
            option.max_quantity = optionFind.max_quantity;
        }
    });

    return modifier;
}
