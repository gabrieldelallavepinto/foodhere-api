import * as functions from "firebase-functions";
import * as firebaseAdmin from 'firebase-admin';

exports.createUser = functions.https.onCall( async (data, context) => {
    
    const email = data.email;
    const password = data.email;

    const authUser = await createUser(email, password);

    return authUser;
});


export async function getUser(email){
    const authUser = await firebaseAdmin.auth().getUserByEmail(email);
    return authUser;
}

export async function createUser(email, password){
    
    const authUser = await firebaseAdmin.auth().createUser({
        email: email,
        emailVerified: false,
        password: password,
        disabled: false
    });

    return authUser;
}




