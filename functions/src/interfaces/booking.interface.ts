/** Interface para la reserva */
export interface BookingInterface {
  /** Identificador único */
  id: string;
​
  /** Identificador único de la empresa */
  businessId: string;
​
  /** Identificador único de la tienda */
  storeId: string;
​
  /** Estado */
  status: BookingStatusEnum;
​
  /** confirmación */
  confirmation: boolean;
​
  /** Fecha de la reserva en formato string 2020-01-24 */
  date: string;
​
  /** identificador del turno */
  shiftId: string;
​
  /** Turno */
  shift: ShiftInterface;
​
  /** Identificador único de la sala */
  roomId: string;
​
  /** Sala / Grupo de mesas */
  room: RoomInterface | null;
​
  /** Identificador de la mesa */
  tableId: string;
​
  /** Mesa */
  table: TableInterface | null;
​
  /** Hora */
  hour: string | null;
​
  /** Número de comensales */
  numDiners: number;
​
  /** Contacto */
  contact: ContactInterface;
​
  /** Notas de la reserva */
  notes: string;
​
  /** Notificar al cliente por email */
  notifyContactEmail: boolean;
​
  /** Fecha en la que se crea la reserva */
  createdAt?: number;
​
  /** Fecha de modificación de la reserva */
  updatedAt?: number;
​
  /** Fecha en que se borra la reserva */
  deletedAt?: number;
}
​
export enum BookingStatusEnum {
  pending = "pending",
  onShow = "onShow",
  noShow = "noShow",
}
  
/** Interface para las salas del restaurante */
export interface RoomInterface {
  /** Identificador único */
  id: string;
​
  /** Nombre de la sala */
  name: string;
​
  /** Descripción de la sala */
  descripcion: string;
​
  /** Fecha en la que se crea la reserva */
  createdAt: number;
​
  /** Fecha de modificación de la reserva */
  updatedAt: number;
​
  /** Fecha en que se borra la reserva */
  deletedAt: number;
}
  
/** Interface para las mesas del restaurante */
export interface TableInterface {
  /** Identificador único */
  id: string;
​
  /** Identificador de la sala */
  roomId: string;
​
  /** Estado de la mesa */
  status: TableStatusEnum;
​
  /** Nombre de la mesa */
  name: string;
​
  /** Número de comensales máximos */
  maxDiners: string;
​
  /** Número de comensales mínimos */
  minDiners: string;
​
  /** Rango de comensales */
  rangeDiners: any;
​
  /** Fecha en la que se crea la reserva */
  createdAt: number;
​
  /** Fecha de modificación de la reserva */
  updatedAt: number;
​
  /** Fecha en que se borra la reserva */
  deletedAt: number;
}
​
export enum TableStatusEnum {
  available = "available",
  unavailable = "unavailable",
}
  
/** Interface para la reserva */
export interface ShiftInterface {
  /** Identificador único */
  id: string;
​
  /** Nombre */
  name: string;
  
  /** Horas  ['12:00', '13:00', '14:00'] */
  hours: string[] | null;
​
  /** Hora mínima */
  minHour: string;
​
  /** Hora máxima */
  maxHour: string;
​
  /** Número de reservas permitidas */
  numBookings: number;
​
  /** Fecha en la que se crea la reserva */
  createAt?: number;
​
  /** Fecha de modificación de la reserva */
  updateAt?: number;
​
  /** Fecha en que se borra la reserva */
  deleteAt?: number;
}
  
/** Interface para los clientes de la reserva */
export interface ContactInterface {
  /** Identificador único */
  id: string;
​
  /** Identificador del usuario */
  userId: string;
​
  /** Nombre */
  firstName: string;
​
  /** Apellidos */
  lastName: string;
​
  /** País */
  state: string;
​
  /** Teléfono */
  phone: string;
​
  /** Email */
  email: string;
​
  /** Etiquetas */
  tags: string[];
  
  /** Notas del cliente */
  notes: string;
​
  /** Ùltima reserva */
  lastBooking?: BookingInterface;
​
  /** Total de reservas */
  totalBookings?: number;
​
  /** Total de reservas canceladas */
  totalCancelBookings?: number;
​
  /** Total de reservas no presentado */
  totalNoShowsBookings?: number;
}